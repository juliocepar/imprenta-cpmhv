<?php

require_once '../dao/AutorDAO.php';
require_once '../models/Autor.php';

$mode = $_REQUEST['mode'];

$autorDAO = new AutorDAO();
$response = [];

if ($mode === 'getAll') {
    $autores = $autorDAO->getAll();
    $response = json_encode($autores);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $autor = new Autor();
        $autor->setNombre($_POST['nombre']);
        $autor->setEstatus(1);
        $id = $autorDAO->insert($autor);
        if ($id !== false) {
            $autor->setId($id);
            $response = ['message' => 'Autor insertado exitosamente', 'result' => $autor->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de autor'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) || !isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $autor = $autorDAO->getById(intval($id));
        $aut = new Autor();
        $aut->setId($autor['id']);
        $aut->setNombre($_POST['nombre']);
        $aut->setEstatus($autor['estatus']);
        if ($autorDAO->update($aut) !== false) {
            $response = ['message' => "Autor #$id actualizado exitosamente", 'result' => $aut->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar el autor #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($autorDAO->deleteL($id) !== false) {
            $response = ['message' => "Autor #$id eliminado exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar el autor #$id"];
        }
    }
    echo json_encode($response);
}

?>
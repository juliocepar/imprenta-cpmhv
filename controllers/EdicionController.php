<?php

require_once '../dao/EdicionDAO.php';
require_once '../models/Edicion.php';

$mode = $_REQUEST['mode'];

$edicionDAO = new EdicionDAO();
$response = [];

if ($mode === 'getAll') {
    $ediciones = $edicionDAO->getAll();
    $response = json_encode($ediciones);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $edicion = new Edicion();
        $edicion->setNombre($_POST['nombre']);
        $edicion->setEstatus(1);
        $id = $edicionDAO->insert($edicion);
        if ($id !== false) {
            $edicion->setId($id);
            $response = ['message' => 'Edición insertada exitosamente', 'result' => $edicion->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de edición'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) || !isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $edicion = $edicionDAO->getById(intval($id));
        $edi = new Edicion();
        $edi->setId($edicion['id']);
        $edi->setNombre($_POST['nombre']);
        $edi->setEstatus($edicion['estatus']);
        if ($edicionDAO->update($edi) !== false) {
            $response = ['message' => "Edición #$id actualizada exitosamente", 'result' => $edi->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar la edición #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($edicionDAO->deleteL($id) !== false) {
            $response = ['message' => "Edición #$id eliminada exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar la edición #$id"];
        }
    }
    echo json_encode($response);
}

?>
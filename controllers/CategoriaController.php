<?php

require_once '../dao/CategoriaDAO.php';
require_once '../dao/TipoCategoriaDAO.php';
require_once '../models/Categoria.php';
require_once '../models/TipoCategoria.php';

$mode = $_REQUEST['mode'];

$categoriaDAO = new CategoriaDAO();
$tipoCategoriaDAO = new TipoCategoriaDAO();
$response = [];

if ($mode === 'getAll') {
    $categorias = $categoriaDAO->getAllSinDiscriminarTipo();
    $response = json_encode($categorias);
    echo $response;
} elseif ($mode === 'getAllMensajes') {
    $categorias = $categoriaDAO->getAll(1);
    $response = json_encode($categorias);
    echo $response;
} elseif ($mode === 'getAllPublicaciones') {
    $categorias = $categoriaDAO->getAll(2);
    $response = json_encode($categorias);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['descripcion']) || !isset($_POST['tipo-categoria'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $tipoCategoria = $tipoCategoriaDAO->getById(intval($_POST['tipo-categoria']));
        $categoria = new Categoria();
        $categoria->setDescripcion($_POST['descripcion']);
        $categoria->setEstatus(1);
        $categoria->setTipoCategoria($tipoCategoria);
        $id = $categoriaDAO->insert($categoria);
        if ($id !== false) {
            $categoria->setId($id);
            $response = ['message' => 'Categoría insertada exitosamente', 'result' => $categoria->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de categoría'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) || !isset($_POST['descripcion']) || !isset($_POST['tipo-categoria'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $tipoCat = $tipoCategoriaDAO->getById(intval($_POST['tipo-categoria']));
        $categoria = $categoriaDAO->getById(intval($id));
        $cat = new Categoria();
        $cat->setId($categoria['id']);
        $cat->setDescripcion($_POST['descripcion']);
        $cat->setEstatus($categoria['estatus']);
        $cat->setTipoCategoria($tipoCat);
        if ($categoriaDAO->update($cat) !== false) {
            $response = ['message' => "Categoría #$id actualizada exitosamente", 'result' => $cat->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar la categoría #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($categoriaDAO->deleteL($id) !== false) {
            $response = ['message' => "Categoría #$id eliminada exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar la categoría #$id"];
        }
    }
    echo json_encode($response);
}

?>
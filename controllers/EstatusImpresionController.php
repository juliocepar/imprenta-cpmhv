<?php

require_once '../dao/EstatusImpresionDAO.php';
require_once '../models/EstatusImpresion.php';

$mode = $_REQUEST['mode'];

$estatusImpresionDAO = new EstatusImpresionDAO();
$response = [];

if ($mode === 'getAll') {
    $estatusimpresion = $estatusImpresionDAO->getAll();
    $response = json_encode($estatusimpresion);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['descripcion'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $estatusimpresion = new EstatusImpresion();
        $estatusimpresion->setDescripcion($_POST['descripcion']);
        $estatusimpresion->setEstatus(1);
        $id = $estatusImpresionDAO->insert($estatusimpresion);
        if ($id !== false) {
            $estatusimpresion->setId($id);
            $response = ['message' => 'Estatus de impresión insertado exitosamente', 'result' => $estatusimpresion->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de estatus de impresión'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) || !isset($_POST['descripcion'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $estatusimpresion = $estatusImpresionDAO->getById(intval($id));
        $est = new EstatusImpresion();
        $est->setId($estatusimpresion['id']);
        $est->setDescripcion($_POST['descripcion']);
        $est->setEstatus($estatusimpresion['estatus']);
        if ($estatusImpresionDAO->update($est) !== false) {
            $response = ['message' => "Estatus de impresión #$id actualizado exitosamente", 'result' => $est->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar el estatus de impresión #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($estatusImpresionDAO->deleteL($id) !== false) {
            $response = ['message' => "Estatus de impresión #$id eliminado exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar el estatus de impresión #$id"];
        }
    }
    echo json_encode($response);
}

?>
<?php

require_once '../dao/MensajeDAO.php';
require_once '../dao/AutorDAO.php';
require_once '../dao/CategoriaDAO.php';
require_once '../dao/EdicionDAO.php';
require_once '../dao/EstatusImpresionDAO.php';
require_once '../dao/ImprentaDAO.php';
require_once '../dao/LugarPredicacionDAO.php';
require_once '../models/Mensaje.php';

$mode = $_REQUEST['mode'];

$mensajeDAO = new MensajeDAO();
$autorDAO = new AutorDAO();
$categoriaDAO = new CategoriaDAO();
$edicionDAO = new EdicionDAO();
$estatusImpresionDAO = new EstatusImpresionDAO();
$imprentaDAO = new ImprentaDAO();
$lugarPredicacionDAO = new LugarPredicacionDAO();
$response = [];

if ($mode === 'getAll') {
    $mensajes = $mensajeDAO->getAll();
    $response = json_encode($mensajes);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['codigo']) ||
        !isset($_POST['titulo']) ||
        !isset($_POST['fecha_predicacion']) ||
        !isset($_POST['ejemplares']) ||
        !isset($_POST['diagramado']) ||
        !isset($_POST['paginas']) ||
        !isset($_POST['tabloides']) ||
        !isset($_POST['autor']) ||
        !isset($_POST['categoria']) ||
        !isset($_POST['edicion']) ||
        !isset($_POST['estatus_impresion']) ||
        !isset($_POST['imprenta']) ||
        !isset($_POST['lugar_predicacion'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $mensaje = new Mensaje();
        $mensaje->setCodigo($_POST['codigo']);
        $mensaje->setTitulo($_POST['titulo']);
        $mensaje->setFechaPredicacion($_POST['fecha_predicacion']);
        $mensaje->setFechaRegistro(Db::_CURRENT_TIMESTAMP);
        $mensaje->setEjemplares($_POST['ejemplares']);
        $mensaje->setDiagramado($_POST['diagramado']);
        $mensaje->setPaginas($_POST['paginas']);
        $mensaje->setTabloides($_POST['tabloides']);
        $mensaje->setEstatus(1);

        $autor = $autorDAO->getById($_POST['autor']);
        $mensaje->setAutor($autor);

        $categoria = $categoriaDAO->getById($_POST['categoria']);
        $mensaje->setCategoria($categoria);

        $edicion = $edicionDAO->getById($_POST['edicion']);
        $mensaje->setEdicion($edicion);

        $estatusImpresion = $estatusImpresionDAO->getById($_POST['estatus_impresion']);
        $mensaje->setEstatusImpresion($estatusImpresion);

        $imprenta = $imprentaDAO->getById($_POST['imprenta']);
        $mensaje->setImprenta($imprenta);

        $lugarPredicacion = $lugarPredicacionDAO->getById($_POST['lugar_predicacion']);
        $mensaje->setLugarPredicacion($lugarPredicacion);

        if (isset($_FILES['archivo'])) {
            if ($_FILES['archivo']['error'] == UPLOAD_ERR_OK && $_FILES['archivo']['name'] != '') {
                $dir_subida = dirname(getcwd()) . '/archivos/mensajes/';
                $dir_subida = str_replace('\\', '/', $dir_subida);
                $dir_relativa = '/' . basename(dirname(getcwd())) . '/archivos/mensajes/';
                $path = pathinfo($_FILES['archivo']['name']);
	            $ext = $path['extension'];
                $archivo_subido = $dir_subida . $mensaje->getCodigo() . ' - ' . $mensaje->getTitulo() . '.' . $ext;
                $archivo = $dir_relativa . $mensaje->getCodigo() . ' - ' . $mensaje->getTitulo() . '.' . $ext;
                if (move_uploaded_file($_FILES['archivo']['tmp_name'], $archivo_subido)) {
                    $mensaje->setArchivo($archivo);
                }
            }
        }

        $id = $mensajeDAO->insert($mensaje);
        if ($id !== false) {
            $mensaje->setId($id);
            $response = ['message' => 'Mensaje insertado exitosamente', 'result' => $mensaje->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de mensaje'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) ||
        !isset($_POST['codigo']) ||
        !isset($_POST['titulo']) ||
        !isset($_POST['fecha_predicacion']) ||
        !isset($_POST['ejemplares']) ||
        !isset($_POST['diagramado']) ||
        !isset($_POST['paginas']) ||
        !isset($_POST['tabloides']) ||
        !isset($_POST['autor']) ||
        !isset($_POST['categoria']) ||
        !isset($_POST['edicion']) ||
        !isset($_POST['estatus_impresion']) ||
        !isset($_POST['imprenta']) ||
        !isset($_POST['lugar_predicacion'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $mensaje = $mensajeDAO->getById(intval($id));
        $men = new Mensaje();
        $men->setId($mensaje['id']);
        $men->setCodigo($_POST['codigo']);
        $men->setTitulo($_POST['titulo']);
        $men->setFechaPredicacion($_POST['fecha_predicacion']);
        $men->setFechaRegistro($mensaje['fechaRegistro']);
        $men->setEjemplares($_POST['ejemplares']);
        $men->setDiagramado($_POST['diagramado']);
        $men->setPaginas($_POST['paginas']);
        $men->setTabloides($_POST['tabloides']);
        $men->setEstatus(1);

        $autor = $autorDAO->getById($_POST['autor']);
        $men->setAutor($autor);

        $categoria = $categoriaDAO->getById($_POST['categoria']);
        $men->setCategoria($categoria);

        $edicion = $edicionDAO->getById($_POST['edicion']);
        $men->setEdicion($edicion);

        $estatusImpresion = $estatusImpresionDAO->getById($_POST['estatus_impresion']);
        $men->setEstatusImpresion($estatusImpresion);

        $imprenta = $imprentaDAO->getById($_POST['imprenta']);
        $men->setImprenta($imprenta);

        $lugarPredicacion = $lugarPredicacionDAO->getById($_POST['lugar_predicacion']);
        $men->setLugarPredicacion($lugarPredicacion);

        if (isset($_FILES['archivo'])) {
            if ($_FILES['archivo']['error'] == UPLOAD_ERR_OK && $_FILES['archivo']['name'] != '') {
                $dir_subida = dirname(getcwd()) . '/archivos/mensajes/';
                $dir_subida = str_replace('\\', '/', $dir_subida);
                $dir_relativa = '/' . basename(dirname(getcwd())) . '/archivos/mensajes/';
                $path = pathinfo($_FILES['archivo']['name']);
	            $ext = $path['extension'];
                $archivo_subido = $dir_subida . $men->getCodigo() . ' - ' . $men->getTitulo() . '.' . $ext;
                $archivo = $dir_relativa . $men->getCodigo() . ' - ' . $men->getTitulo() . '.' . $ext;
                if (move_uploaded_file($_FILES['archivo']['tmp_name'], $archivo_subido)) {
                    $men->setArchivo($archivo);
                }
            }
        }

        if ($mensajeDAO->update($men) !== false) {
            $response = ['message' => "Mensaje " . $_POST['codigo'] . " actualizado exitosamente", 'result' => $men->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar el mensaje " . $_POST['codigo']];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($mensajeDAO->deleteL($id) !== false) {
            $response = ['message' => "Mensaje eliminado exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar el mensaje"];
        }
    }
    echo json_encode($response);
}

?>
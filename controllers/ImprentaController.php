<?php

require_once '../dao/ImprentaDAO.php';
require_once '../models/Imprenta.php';

$mode = $_REQUEST['mode'];

$imprentaDAO = new ImprentaDAO();
$response = [];

if ($mode === 'getAll') {
    $imprentas = $imprentaDAO->getAll();
    $response = json_encode($imprentas);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['codigo']) || !isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $imprenta = new Imprenta();
        $imprenta->setCodigo($_POST['codigo']);
        $imprenta->setNombre($_POST['nombre']);
        $imprenta->setEstatus(1);
        $id = $imprentaDAO->insert($imprenta);
        if ($id !== false) {
            $imprenta->setId($id);
            $response = ['message' => 'Imprenta insertada exitosamente', 'result' => $imprenta->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de imprenta'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) || !isset($_POST['codigo']) || !isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $imprenta = $imprentaDAO->getById(intval($id));
        $edi = new Imprenta();
        $edi->setId($imprenta['id']);
        $edi->setCodigo($_POST['codigo']);
        $edi->setNombre($_POST['nombre']);
        $edi->setEstatus($imprenta['estatus']);
        if ($imprentaDAO->update($edi) !== false) {
            $response = ['message' => "Imprenta #$id actualizada exitosamente", 'result' => $edi->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar la imprenta #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($imprentaDAO->deleteL($id) !== false) {
            $response = ['message' => "Imprenta #$id eliminada exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar la imprenta #$id"];
        }
    }
    echo json_encode($response);
}

?>
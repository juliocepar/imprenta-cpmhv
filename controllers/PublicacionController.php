<?php

require_once '../dao/PublicacionDAO.php';
require_once '../dao/CategoriaDAO.php';
require_once '../dao/ImprentaDAO.php';
require_once '../dao/EstatusImpresionDAO.php';
require_once '../models/Publicacion.php';

$mode = $_REQUEST['mode'];

$publicacionDAO = new PublicacionDAO();
$categoriaDAO = new CategoriaDAO();
$imprentaDAO = new ImprentaDAO();
$estatusImpresionDAO = new EstatusImpresionDAO();
$response = [];

if ($mode === 'getAll') {
    $publicacions = $publicacionDAO->getAll();
    $response = json_encode($publicacions);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['titulo']) ||
        !isset($_POST['ejemplares']) ||
        !isset($_POST['diagramado']) ||
        !isset($_POST['paginas']) ||
        !isset($_POST['tabloides']) ||
        !isset($_POST['categoria']) ||
        !isset($_POST['imprenta']) ||
        !isset($_POST['estatus_impresion'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $publicacion = new Publicacion();
        $publicacion->setTitulo($_POST['titulo']);
        $publicacion->setFechaRegistro(Db::_CURRENT_TIMESTAMP);
        $publicacion->setEjemplares($_POST['ejemplares']);
        $publicacion->setDiagramado($_POST['diagramado']);
        $publicacion->setPaginas($_POST['paginas']);
        $publicacion->setTabloides($_POST['tabloides']);
        $publicacion->setEstatus(1);

        $categoria = $categoriaDAO->getById($_POST['categoria']);
        $publicacion->setCategoria($categoria);

        $imprenta = $imprentaDAO->getById($_POST['imprenta']);
        $publicacion->setImprenta($imprenta);
        
        $estatusImpresion = $estatusImpresionDAO->getById($_POST['estatus_impresion']);
        $publicacion->setEstatusImpresion($estatusImpresion);

        if (isset($_FILES['archivo'])) {
            if ($_FILES['archivo']['error'] == UPLOAD_ERR_OK && $_FILES['archivo']['name'] != '') {
                $dir_subida = dirname(getcwd()) . '/archivos/publicaciones/';
                $dir_subida = str_replace('\\', '/', $dir_subida);
                $dir_relativa = '/' . basename(dirname(getcwd())) . '/archivos/publicaciones/';
                $path = pathinfo($_FILES['archivo']['name']);
	            $ext = $path['extension'];
                $archivo_subido = $dir_subida . $publicacion->getTitulo() . '.' . $ext;
                $archivo = $dir_relativa . $publicacion->getTitulo() . '.' . $ext;
                if (move_uploaded_file($_FILES['archivo']['tmp_name'], $archivo_subido)) {
                    $publicacion->setArchivo($archivo);
                }
            }
        }

        $id = $publicacionDAO->insert($publicacion);
        if ($id !== false) {
            $publicacion->setId($id);
            $response = ['message' => 'Publicación insertada exitosamente', 'result' => $publicacion->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de publicación'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) ||
        !isset($_POST['titulo']) ||
        !isset($_POST['ejemplares']) ||
        !isset($_POST['diagramado']) ||
        !isset($_POST['paginas']) ||
        !isset($_POST['tabloides']) ||
        !isset($_POST['categoria']) ||
        !isset($_POST['imprenta']) ||
        !isset($_POST['estatus_impresion'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $publicacion = $publicacionDAO->getById(intval($id));
        $pub = new Publicacion();
        $pub->setId($publicacion['id']);
        $pub->setTitulo($_POST['titulo']);
        $pub->setFechaRegistro($publicacion['fechaRegistro']);
        $pub->setEjemplares($_POST['ejemplares']);
        $pub->setDiagramado($_POST['diagramado']);
        $pub->setPaginas($_POST['paginas']);
        $pub->setTabloides($_POST['tabloides']);
        $pub->setEstatus(1);

        $categoria = $categoriaDAO->getById($_POST['categoria']);
        $pub->setCategoria($categoria);

        $imprenta = $imprentaDAO->getById($_POST['imprenta']);
        $pub->setImprenta($imprenta);
        
        $estatusImpresion = $estatusImpresionDAO->getById($_POST['estatus_impresion']);
        $pub->setEstatusImpresion($estatusImpresion);

        if (isset($_FILES['archivo'])) {
            if ($_FILES['archivo']['error'] == UPLOAD_ERR_OK && $_FILES['archivo']['name'] != '') {
                $dir_subida = dirname(getcwd()) . '/archivos/publicaciones/';
                $dir_subida = str_replace('\\', '/', $dir_subida);
                $dir_relativa = '/' . basename(dirname(getcwd())) . '/archivos/publicaciones/';
                $path = pathinfo($_FILES['archivo']['name']);
	            $ext = $path['extension'];
                $archivo_subido = $dir_subida . $pub->getTitulo() . '.' . $ext;
                $archivo = $dir_relativa . $pub->getTitulo() . '.' . $ext;
                if (move_uploaded_file($_FILES['archivo']['tmp_name'], $archivo_subido)) {
                    $pub->setArchivo($archivo);
                }
            }
        }
        
        if ($publicacionDAO->update($pub) !== false) {
            $response = ['message' => "Publicación #$id actualizada exitosamente", 'result' => $pub->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar la publicación #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($publicacionDAO->deleteL($id) !== false) {
            $response = ['message' => "Publicación #$id eliminada exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar la publicación #$id"];
        }
    }
    echo json_encode($response);
}

?>
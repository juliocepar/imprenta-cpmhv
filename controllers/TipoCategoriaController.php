<?php

require_once '../dao/TipoCategoriaDAO.php';
require_once '../models/TipoCategoria.php';

$mode = $_REQUEST['mode'];

$tipoCategoriaDAO = new TipoCategoriaDAO();

if ($mode === 'getAll') {
    $tiposCategorias = $tipoCategoriaDAO->getAll();
    $response = json_encode($tiposCategorias);
    echo $response;
}

?>
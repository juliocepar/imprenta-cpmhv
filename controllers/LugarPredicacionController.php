<?php

require_once '../dao/LugarPredicacionDAO.php';
require_once '../models/LugarPredicacion.php';

$mode = $_REQUEST['mode'];

$lugarPredicacionDAO = new LugarPredicacionDAO();
$response = [];

if ($mode === 'getAll') {
    $lugarespredicacion = $lugarPredicacionDAO->getAll();
    $response = json_encode($lugarespredicacion);
    echo $response;
} elseif ($mode === 'insert') {
    if (!isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $lugarpredicacion = new LugarPredicacion();
        $lugarpredicacion->setNombre($_POST['nombre']);
        $lugarpredicacion->setEstatus(1);
        $id = $lugarPredicacionDAO->insert($lugarpredicacion);
        if ($id !== false) {
            $lugarpredicacion->setId($id);
            $response = ['message' => 'Lugar de predicación insertado exitosamente', 'result' => $lugarpredicacion->expose()];
        } else {
            $response = ['error' => true, 'message' => 'Hubo un problema al insertar el nuevo registro de lugar de predicación'];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'update') {
    if (!isset($_POST['id']) || !isset($_POST['nombre'])) {
        $response = ['error' => true, 'message' => 'Faltan datos'];
    } else {
        $id = $_POST['id'];
        $lugarpredicacion = $lugarPredicacionDAO->getById(intval($id));
        $lug = new LugarPredicacion();
        $lug->setId($lugarpredicacion['id']);
        $lug->setNombre($_POST['nombre']);
        $lug->setEstatus($lugarpredicacion['estatus']);
        if ($lugarPredicacionDAO->update($lug) !== false) {
            $response = ['message' => "Lugar de predicación #$id actualizado exitosamente", 'result' => $lug->expose()];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al actualizar el lugar de predicación #$id"];
        }
    }
    echo json_encode($response);
} elseif ($mode === 'delete') {
    if (!isset($_GET['id'])) {
        $response = ['error' => true, 'message' => 'id inválido'];
    } else {
        $id = $_GET['id'];
        if ($lugarPredicacionDAO->deleteL($id) !== false) {
            $response = ['message' => "Lugar de predicación #$id eliminado exitosamente"];
        } else {
            $response = ['error' => true, 'message' => "Hubo un problema al eliminar el lugar de predicación #$id"];
        }
    }
    echo json_encode($response);
}

?>
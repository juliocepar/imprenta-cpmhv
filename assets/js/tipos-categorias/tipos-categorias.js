$(document).ready(function () {
    $('[data-target="tooltip"]').tooltip();
    inicializarDataTable('tabTiposCategorias');
});

function editar(elem) {
    let i = 0;
    let tr = $(elem).parents('tr');
    if (!tr.attr('id') || tr.attr('id') === '') {
        tr.attr('id', makeId(10));
    }
    let idTr = tr.attr('id');
    tr.children('td').each(function (idx, td) {
        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
            let oldValue = $(td).text().trim();
            if ($(td).hasClass('texto')) {
                $(td).html(
                    `<input type="text" class="form-control" id="${idTr + '-' + i}" data-old-value="${oldValue}" value="${oldValue}" />`
                );
            }
        }
        i++;
    });
    $(`#${idTr} td.acciones button`).toggleClass('d-none');
}

function cancelarEdicion(elem) {
    let i = 0;
    let tr = $(elem).parents('tr');
    let idTr = tr.attr('id');
    tr.children('td').each(function (idx, td) {
        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
            let oldValue = $(`#${idTr}-${i}`).data('old-value');
            if ($(td).hasClass('texto')) {
                $(td).html(oldValue);
            }
        }
        i++;
    });
    $(`#${idTr} td.acciones button`).toggleClass('d-none');
}

function guardarEdicion(elem) {
    let i = 0;
    let tr = $(elem).parents('tr');
    let idTr = tr.attr('id');
    let valid = true;
    tr.children('td').each(function (idx, td) {
        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
            let newValue = $(`#${idTr}-${i}`).val();
            if (newValue.trim() === '') {
                valid = false;
                return false;
            }
            if ($(td).hasClass('texto')) {
                $(td).html(newValue);
            }
        }
        i++;
    });
    if (valid) {
        $(`#${idTr} td.acciones button`).toggleClass('d-none');
        alertarExito('¡Edición exitosa!', '');
    } else {
        alertarAdvertencia('Complete todos los campos', '');
    }
}

function eliminar(elem) {
    let accionSiExito = function () {
        let row = $('#tabTiposCategorias').DataTable().row($(elem).parents('tr'));
        row.remove().draw(false);
        alertarExito('¡Éxito!', 'El tipo de categoría fue eliminado exitosamente');
    };
    confirmarAlerta(
        '¿Seguro?',
        '¿Seguro de que desea eliminar este tipo de categoría?',
        accionSiExito
    );
}

function registrar() {
    $("#modalRegistrarTipoCategoria").modal('toggle');
}

function guardarRegistro() {
    let valido = true;
    $("#modalRegistrarTipoCategoria div.modal-body input").each(function (idx, input) {
        if ($(input).val().trim() === '') {
            valido = false;
            return false;
        }
    });
    if (!valido) {
        alertarAdvertencia('Complete todos los campos', '');
        return;
    }
    $("#modalRegistrarTipoCategoria").modal('toggle');
    alertarExito('¡Registro exitoso!', '');
}

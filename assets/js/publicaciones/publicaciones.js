const HOST_PUBLICACION = '../controllers/PublicacionController.php';
const HOST_CATEGORIA = '../controllers/CategoriaController.php';
const HOST_ESTATUS_IMPRESION = '../controllers/EstatusImpresionController.php';
const HOST_IMPRENTA = '../controllers/ImprentaController.php';

$(document).ready(function () {

    getPublicaciones().then(publicaciones => {
        cargarPublicaciones(publicaciones);
        getCategorias().then(categorias => {
            getEstatusImpresion().then(estatus => {
                getImprentas().then(imprentas => {
                    let select = $('select[name="imprenta"]');
                    select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                    for (let imprenta of imprentas) {
                        $(select).append(`<option value="${imprenta.id}">${imprenta.nombre}</option>`);
                    }
                });
                let select = $('select[name="estatus_impresion"]');
                select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                for (let est of estatus) {
                    $(select).append(`<option value="${est.id}">${est.descripcion}</option>`);
                }
            });
            let select = $('select[name="categoria"]');
            select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
            for (let categoria of categorias) {
                $(select).append(`<option value="${categoria.id}">${categoria.descripcion}</option>`);
            }
        });
    });

    $("#formCrearPublicacion").on('submit', function (ev) {
        ev.preventDefault();
        let fd = new FormData(this);
        fd.append('mode', 'insert');
        $.ajax({
            method: 'POST',
            url: HOST_PUBLICACION,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: fd,
            success: function (response) {
                if (response.error) {
                    alertarError("Error", response.message);
                } else {
                    alertarExito("¡Éxito!", response.message);
                    $("#modalRegistrarPublicacion").modal('hide');
                    getPublicaciones().then(publicaciones => {
                        cargarPublicaciones(publicaciones);
                    });
                }
            },
            error: function (xhr) {
                console.error(xhr.responseJSON);
                alertarError("Error", "Hubo un problema al tratar de registrar una publicación");
            }
        });
    });

    $("#modalRegistrarPublicacion").on('hidden.bs.modal', function (e) {
        $("#formCrearPublicacion")[0].reset();
    });

    $("#formEditarPublicacion").on('submit', function (ev) {
        ev.preventDefault();
        let fd = new FormData(this);
        fd.append('mode', 'update');
        $.ajax({
            method: 'POST',
            url: HOST_PUBLICACION,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: fd,
            success: function (response) {
                if (response.error) {
                    alertarError("Error", response.message);
                } else {
                    alertarExito("¡Éxito!", response.message);
                    $("#modalDetallePublicacion").modal('hide');
                    getPublicaciones().then(mensajes => {
                        cargarPublicaciones(mensajes);
                    });
                }
            },
            error: function (xhr) {
                console.error(xhr.responseJSON);
                alertarError("Error", "Hubo un problema al tratar de editar una publicación");
            }
        });
    });

    $("#modalDetallePublicacion").on('hidden.bs.modal', function (e) {
        cancelarEdicion();
    });

    $("#subirArchivoDet").on('change', function (e) {
        if (this.checked) {
            $("#archivoDet").removeClass('d-none');
        } else {
            $("#archivoDet").addClass('d-none');
            $("#archivoDet").val('')
        }
    });
});

function cargarPublicaciones(publicaciones) {
    let idTabla = 'tabPublicaciones';
    //$(`#${idTabla} tbody`).empty();
    if ($.fn.DataTable.isDataTable(`#${idTabla}`)) {
        $(`#${idTabla}`).DataTable().clear().destroy();
    }
    let publicacionesObj = {};
    for (let pub of publicaciones) {
        $(`#${idTabla} tbody`).append(`<tr id="mensaje-${pub.id}"></tr>`);
        publicacionesObj[pub.id] = pub;
        $(`#mensaje-${pub.id}`).append(
            `<td class="id">${pub.id}</td>
            <td class="texto">${pub.categoria.descripcion}</td>
            <td class="texto">${pub.titulo}</td>
            <td class="texto">${pub.diagramado == "1" ? 'Sí': 'No'}</td>
            <td class="texto">${pub.paginas}</td>
            <td class="texto">${pub.tabloides}</td>
            <td class="url">${pub.archivo ? '<a href="' + pub.archivo + '" target="_blank">Ver archivo</a>': 'No disponible'}</td>
            <td class="acciones">
                <button class="btn btn-primary btn-sm btn-fab btn-icon btn-round"
                    onclick='mostrarDetallePublicacion(this, ${pub.id})' data-target="tooltip" data-placement="left"
                    title="Ver detalles">
                    <i class="fa fa-search"></i>
                </button>
            </td>`
        );
    }
    sessionStorage.setItem('publicaciones', JSON.stringify(publicacionesObj));
    $('[data-target="tooltip"]').tooltip();
    inicializarDataTable(idTabla).columns(0).order('desc').draw();
}

async function getPublicaciones() {
    try {
        const response = await fetch(HOST_PUBLICACION + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getCategorias() {
    try {
        const response = await fetch(HOST_CATEGORIA + "?mode=getAllPublicaciones");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getEstatusImpresion() {
    try {
        const response = await fetch(HOST_ESTATUS_IMPRESION + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getImprentas() {
    try {
        const response = await fetch(HOST_IMPRENTA + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

function mostrarDetallePublicacion(elem, idPublicacion) {
    let publicaciones = JSON.parse(sessionStorage.getItem('publicaciones'));
    let pub = publicaciones[idPublicacion];
    $("#idDet").val(pub.id);
    $("#tituloDet").val(pub.titulo);
    $("#categoriaDetTxt").val(pub.categoria.descripcion);
    $("#fechaDet").val(pub.fechaRegistro);
    $("#estatusDetTxt").val(pub.estatusImpresion.descripcion);
    $("#diagramadoDetTxt").val(pub.diagramado == "1" ? 'Sí': 'No');
    $("#paginasDet").val(pub.paginas);
    $("#tabloidesDet").val(pub.tabloides);
    $("#ejemplaresDet").val(pub.ejemplares);
    $("#imprentaDetTxt").val(pub.imprenta.nombre);
    if (pub.archivo) {
        let archivo_split = pub.archivo.split('/');
        let nombre_archivo = archivo_split[archivo_split.length - 1];
        $("#nombreArchivoDet").html(`<a href="${pub.archivo}" target="_blank">${nombre_archivo}</a>`)
    } else {
        $("#nombreArchivoDet").text("No disponible");
    }
    $("#archivoDet").addClass("d-none");

    $("#modalDetallePublicacion").modal('toggle');
}

function registrarPublicacion(elem) {
    $("#modalRegistrarPublicacion").modal('toggle');
}

function editarPublicacion() {
    $("#modalDetallePublicacion input.editable").each(function (index, input) {
        input["data-old"] = input.value;
        $(input).prop("readonly", false);
    });
    $("#modalDetallePublicacion input.selectable").each(function (index, input) {
        let idSelect = input.id.substring(0, input.id.indexOf("Txt"));
        $(`#${idSelect} option`).each(function (index, option) {
            if ($(option).text() === input.value) {
                $(option).prop("selected", true);
                return false;
            }
        });
        $(`#${idSelect}, #${input.id}`).toggleClass("d-none");
    });
    $("#modalDetallePublicacion #btnEliminarPublicacion, #modalDetallePublicacion #btnEditarPublicacion").addClass("d-none");
    $("#modalDetallePublicacion #btnCancelarEdicion, #modalDetallePublicacion #btnGuardarCambios, #subirNuevoArchivoDiv").removeClass("d-none");
}

function cancelarEdicion() {
    $("#modalDetallePublicacion input.editable").each(function (index, input) {
        input.value = input["data-old"];
        $(input).prop("readonly", true);
    });
    $("#modalDetallePublicacion input.selectable").each(function (index, input) {
        let idSelect = input.id.substring(0, input.id.indexOf("Txt"));
        $(`#${idSelect}`).addClass("d-none");
        $(`#${input.id}`).removeClass("d-none");
    });
    $("#modalDetallePublicacion #btnEliminarPublicacion, #modalDetallePublicacion #btnEditarPublicacion, #nombreArchivoDet").removeClass("d-none");
    $("#modalDetallePublicacion #btnCancelarEdicion, #modalDetallePublicacion #btnGuardarCambios, #archivoDet, #subirNuevoArchivoDiv").addClass("d-none");
    $("#subirArchivoDet").prop('checked', false);
    $("#archivoDet").val('');
}

function eliminarPublicacion() {
    let accionSiExito = function () {
        getPublicaciones().then(publicaciones => {
            cargarPublicaciones(publicaciones);
            alertarExito('¡Éxito!', 'La publicación fue eliminada exitosamente');
            $("#modalDetallePublicacion").modal('hide');
        });
    };
    let accionAjax = function () {
        let id = $(`#idDet`).val();
        return fetch(HOST_PUBLICACION + `?mode=delete&id=${id}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText)
                }
                return response.json()
            })
            .then(respJson => {
                if (respJson.error) {
                    Swal.showValidationMessage(
                        `Request failed: ${respJson.message}`
                    );
                } else {
                    return respJson;
                }
            })
            .catch(error => {
                Swal.showValidationMessage(
                    `Request failed: ${error}`
                )
            });
    };
    confirmarAlerta(
        '¿Seguro?',
        '¿Seguro de que desea eliminar esta publicación?',
        accionSiExito,
        accionAjax
    );
}
$(document).ready(function () {
    $('#navbar').load('templates/navbar.html');
    $('#menu').load('templates/menu.html', function () {
        $('#menu ul.nav li a').each(function (ixd, anchor) {
            let a = $(anchor).attr('href').split('/');
            let currPage = window.location.pathname.split('/');
            if (a[a.length - 1] == currPage[currPage.length - 1]) {
                $(anchor).parents('li').addClass('active');
                return false;
            }
        });
    });
    $('#footer').load('templates/footer.html', function () {
        $('#year').html(`${new Date().getFullYear()}`);
    });
});

function makeId(length = 16) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function quitarAcentos(texto) {
    return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

function snakeToCamel(str) {
    return str.replace(
        /([-_][a-z])/g,
        (group) => group.toUpperCase()
            .replace('-', '')
            .replace('_', '')
    );
}

function inicializarDataTable(id) {
    return $(`#${id}`).DataTable({
        responsive: true,
        language: {
            url: "../assets/Spanish.json"
        }
    });
}

function alertarExito(titulo, mensaje) {
    Swal.fire(titulo, mensaje, 'success');
}

function alertarAdvertencia(titulo, mensaje) {
    Swal.fire(titulo, mensaje, 'warning');
}

function alertarError(titulo, mensaje) {
    Swal.fire(titulo, mensaje, 'error');
}

function confirmarAlerta(titulo, descripcion, accionSiExito = function () { }, accionAjax = function () { }) {
    Swal.fire({
        title: titulo,
        text: descripcion,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, continuar',
        cancelButtonText: 'Cancelar',
        showLoaderOnConfirm: true,
        preConfirm: () => {
            if (accionAjax) {
                return accionAjax();
            } else {
                return true;
            }
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            accionSiExito();
        }
    })
}
const HOST_ESTATUS_IMPRESION = '../controllers/EstatusImpresionController.php';

$(document).ready(function () {
    let idTabla = 'tabEstatusImpresion';
    getEstatusImpresion().then(estatusImpresion => {
        $(`#${idTabla} tbody`).empty();
        let campoEditable = 'descripcion';
        for (let est of estatusImpresion) {
            $(`#${idTabla} tbody`).append(`<tr id="estatusImpresion-${est.id}"></tr>`);
            for (let campo in est) {
                let clase = 'texto';
                let texto = est[campo];
                if (campo == 'id') {
                    clase = 'id';
                } else if (campo == 'estatus') {
                    clase = 'estatus';
                    texto = parseInt(est[campo]) ? 'Activo' : 'Eliminado';
                }
                $(`#estatusImpresion-${est.id}`).append(`<td class="${clase}">${texto}</td>`);
            }
            $(`#estatusImpresion-${est.id}`).append(
                `<td class="acciones">
                    <button class="btn btn-primary btn-sm btn-fab btn-icon btn-round"
                        onclick="editar(this, '${campoEditable}')" data-target="tooltip" data-placement="left"
                        title="Editar">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button class="btn btn-danger btn-sm btn-fab btn-icon btn-round" onclick="eliminar(this)"
                        data-target="tooltip" data-placement="left" title="Eliminar">
                        <i class="fa fa-trash"></i>
                    </button>
                    <button class="btn btn-success btn-sm btn-fab btn-icon btn-round d-none"
                        onclick="guardarEdicion(this)" data-target="tooltip" data-placement="left" title="Guardar">
                        <i class="fa fa-save"></i>
                    </button>
                    <button class="btn btn-danger btn-sm btn-fab btn-icon btn-round d-none"
                        onclick="cancelarEdicion(this)" data-target="tooltip" data-placement="left"
                        title="Cancelar">
                        <i class="fa fa-times"></i>
                    </button>
                </td>`
            );
        }
        $('[data-target="tooltip"]').tooltip();
        inicializarDataTable(idTabla);
    });
});

async function getEstatusImpresion() {
    try {
        const response = await fetch(HOST_ESTATUS_IMPRESION + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

function editar(elem) {
    let i = 0;
    let tr = $(elem).parents('tr');
    if (!tr.attr('id') || tr.attr('id') === '') {
        tr.attr('id', makeId(10));
    }
    let idTr = tr.attr('id');
    tr.children('td').each(function (idx, td) {
        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
            let oldValue = $(td).text().trim();
            if ($(td).hasClass('texto')) {
                $(td).html(
                    `<input type="text" class="form-control" id="${idTr + '-' + i}" data-old-value="${oldValue}" value="${oldValue}" />`
                );
            }
        }
        i++;
    });
    $(`#${idTr} td.acciones button`).toggleClass('d-none');
}

function cancelarEdicion(elem) {
    let i = 0;
    let tr = $(elem).parents('tr');
    let idTr = tr.attr('id');
    tr.children('td').each(function (idx, td) {
        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
            let oldValue = $(`#${idTr}-${i}`).data('old-value');
            if ($(td).hasClass('texto')) {
                $(td).html(oldValue);
            }
        }
        i++;
    });
    $(`#${idTr} td.acciones button`).toggleClass('d-none');
}

function guardarEdicion(elem) {
    let i = 0;
    let tr = $(elem).parents('tr');
    let idTr = tr.attr('id');
    let valid = true;
    let tabla = tr.parents('table');
    let columnasCabecera = tabla.children('thead').children('tr').children('th');
    let valores = { id: idTr.split('-')[1], mode: 'update' };
    tr.children('td').each(function (idx, td) {
        let nombreColumna = $(columnasCabecera[i]).data('nombre-accesible');
        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
            let newValue = $(`#${idTr}-${i}`).val();
            if (newValue.trim() === '') {
                valid = false;
                return false;
            }
            valores[nombreColumna] = newValue;
            /*if ($(td).hasClass('texto')) {
                $(td).html(newValue);
            }*/
        }
        i++;
    });
    let fd = new FormData();
    for (let clave in valores) {
        fd.append(clave, valores[clave]);
    }
    if (valid) {
        $.ajax({
            method: "POST",
            url: HOST_ESTATUS_IMPRESION,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: fd,
            success: function (response) {
                if (response.error) {
                    alertarError('Error', response.message);
                } else {
                    i = 0;
                    let cat = response.result;
                    tr.children('td').each(function (idx, td) {
                        let nombreColumna = $(columnasCabecera[i]).data('nombre-accesible');
                        if (!$(td).hasClass('id') && !$(td).hasClass('acciones') && !$(td).hasClass('estatus')) {
                            if ($(td).hasClass('texto')) {
                                $(td).html(cat[snakeToCamel(nombreColumna)]);
                            } else {
                                $(td).html(cat[snakeToCamel(nombreColumna)].descripcion);
                            }
                        }
                        i++;
                    });
                    $(`#${idTr} td.acciones button`).toggleClass('d-none');
                    alertarExito('¡Edición exitosa!', '');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.responseJson);
                alertarError('Error', 'Ocurrió un problema al editar el estatus de impresión');
            }
        });
    } else {
        alertarAdvertencia('Complete todos los campos', '');
    }
}

function eliminar(elem) {
    let accionSiExito = function () {
        let row = $('#tabEstatusImpresion').DataTable().row($(elem).parents('tr'));
        row.remove().draw(false);
        alertarExito('¡Éxito!', 'El estatus de impresión fue eliminado exitosamente');
    };
    let accionAjax = function () {
        let id = $(elem).parents('tr').attr('id').split('-')[1];
        return fetch(HOST_ESTATUS_IMPRESION + `?mode=delete&id=${id}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText)
                }
                return response.json()
            })
            .then(respJson => {
                if (respJson.error) {
                    Swal.showValidationMessage(
                        `Request failed: ${respJson.message}`
                    );
                } else {
                    return respJson;
                }
            })
            .catch(error => {
                Swal.showValidationMessage(
                    `Request failed: ${error}`
                )
            });
    };
    confirmarAlerta(
        '¿Seguro?',
        '¿Seguro de que desea eliminar este estatus de impresión?',
        accionSiExito,
        accionAjax
    );
}

function registrar() {
    $("#modalRegistrarEstatusImpresion").modal('toggle');
}

function guardarRegistro() {
    let valido = true;
    $("#modalRegistrarEstatusImpresion div.modal-body input").each(function (idx, input) {
        if ($(input).val().trim() === '') {
            alertarAdvertencia('Complete todos los campos', '');
            $(input).focus();
            valido = false;
            return false;
        }
    });
    if (valido) {
        let form = new FormData(document.getElementById('formCEI'));
        form.append('mode', 'insert');
        $.ajax({
            method: 'POST',
            url: HOST_ESTATUS_IMPRESION,
            dataType: 'json',
            data: form,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.error) {
                    alertarError('Error', response.message);
                } else {
                    alertarExito('¡Registro exitoso!', response.message);
                    let campoEditable = 'nombre';
                    let cat = response.result;
                    let clasesPorFila = [];
                    let valoresPorFila = [];
                    for (let campo in cat) {
                        let clase = 'texto';
                        let texto = cat[campo];
                        if (campo == 'id') {
                            clase = 'id';
                        } else if (campo == 'estatus') {
                            clase = 'estatus';
                            texto = parseInt(cat[campo]) ? 'Activo' : 'Eliminado';
                        }
                        clasesPorFila.push(clase);
                        valoresPorFila.push(texto);
                    }
                    valoresPorFila.push(
                        `<td class="acciones">
                                <button class="btn btn-primary btn-sm btn-fab btn-icon btn-round"
                                    onclick="editar(this, '${campoEditable}')" data-target="tooltip" data-placement="left"
                                    title="Editar">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button class="btn btn-danger btn-sm btn-fab btn-icon btn-round" onclick="eliminar(this)"
                                    data-target="tooltip" data-placement="left" title="Eliminar">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <button class="btn btn-success btn-sm btn-fab btn-icon btn-round d-none"
                                    onclick="guardarEdicion(this)" data-target="tooltip" data-placement="left" title="Guardar">
                                    <i class="fa fa-save"></i>
                                </button>
                                <button class="btn btn-danger btn-sm btn-fab btn-icon btn-round d-none"
                                    onclick="cancelarEdicion(this)" data-target="tooltip" data-placement="left"
                                    title="Cancelar">
                                    <i class="fa fa-times"></i>
                                </button>
                            </td>`
                    );
                    clasesPorFila.push('acciones');
                    let nuevaFila = $("#tabEstatusImpresion").DataTable().row.add(valoresPorFila).draw(false).node();
                    $(nuevaFila).attr('id', `estatusImpresion-${cat.id}`);
                    let fila = $(nuevaFila).children('td');
                    fila.each(function (i, td) {
                        $(td).addClass(clasesPorFila[i]);
                    });
                    $("#modalRegistrarEstatusImpresion").modal('toggle');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.responseJson);
                alertarError('Error', 'Ocurrió un problema al tratar de crear un nuevo estatus de impresión');
            }
        });
    }
}

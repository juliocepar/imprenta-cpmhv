const HOST_MENSAJE = '../controllers/MensajeController.php';
const HOST_AUTOR = '../controllers/AutorController.php';
const HOST_LUGAR_PREDICACION = '../controllers/LugarPredicacionController.php';
const HOST_EDICION = '../controllers/EdicionController.php';
const HOST_ESTATUS_IMPRESION = '../controllers/EstatusImpresionController.php';
const HOST_IMPRENTA = '../controllers/ImprentaController.php';
const HOST_CATEGORIA = '../controllers/CategoriaController.php';

$(document).ready(function () {

    getMensajes().then(mensajes => {
        cargarMensajes(mensajes);
        getAutores().then(autores => {
            getLugaresPredicaciones().then(lugares => {
                getEdiciones().then(ediciones => {
                    getEstatusImpresion().then(estatus => {
                        getImprentas().then(imprentas => {
                            getCategorias().then(categorias => {
                                let select = $('select[name="categoria"]');
                                select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                                for (let categoria of categorias) {
                                    $(select).append(`<option value="${categoria.id}">${categoria.descripcion}</option>`);
                                }
                            });
                            let select = $('select[name="imprenta"]');
                            select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                            for (let imprenta of imprentas) {
                                $(select).append(`<option value="${imprenta.id}">${imprenta.nombre}</option>`);
                            }
                        });
                        let select = $('select[name="estatus_impresion"]');
                        select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                        for (let est of estatus) {
                            $(select).append(`<option value="${est.id}">${est.descripcion}</option>`);
                        }
                    });
                    let select = $('select[name="edicion"]');
                    select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                    for (let edicion of ediciones) {
                        $(select).append(`<option value="${edicion.id}">${edicion.nombre}</option>`);
                    }
                });
                let select = $('select[name="lugar_predicacion"]');
                select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
                for (let lugar of lugares) {
                    $(select).append(`<option value="${lugar.id}">${lugar.nombre}</option>`);
                }
            });
            let select = $('select[name="autor"]');
            select.html('<option value="" disabled hidden selected>-- Seleccione --</option>');
            for (let autor of autores) {
                $(select).append(`<option value="${autor.id}">${autor.nombre}</option>`);
            }
        });
    });

    $("#formCrearMensaje").on('submit', function (ev) {
        ev.preventDefault();
        let fd = new FormData(this);
        fd.append('mode', 'insert');
        $.ajax({
            method: 'POST',
            url: HOST_MENSAJE,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: fd,
            success: function (response) {
                if (response.error) {
                    alertarError("Error", response.message);
                } else {
                    alertarExito("¡Éxito!", response.message);
                    $("#modalRegistrarMensaje").modal('hide');
                    getMensajes().then(mensajes => {
                        cargarMensajes(mensajes);
                    });
                }
            },
            error: function (xhr) {
                console.error(xhr.responseJSON);
                alertarError("Error", "Hubo un problema al tratar de registrar un mensaje");
            }
        });
    });

    $("#modalRegistrarMensaje").on('hidden.bs.modal', function (e) {
        $("#formCrearMensaje")[0].reset();
    });

    $("#formEditarMensaje").on('submit', function (ev) {
        ev.preventDefault();
        let fd = new FormData(this);
        fd.append('mode', 'update');
        $.ajax({
            method: 'POST',
            url: HOST_MENSAJE,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: fd,
            success: function (response) {
                if (response.error) {
                    alertarError("Error", response.message);
                } else {
                    alertarExito("¡Éxito!", response.message);
                    $("#modalDetalleMensaje").modal('hide');
                    getMensajes().then(mensajes => {
                        cargarMensajes(mensajes);
                    });
                }
            },
            error: function (xhr) {
                console.error(xhr.responseJSON);
                alertarError("Error", "Hubo un problema al tratar de editar un mensaje");
            }
        });
    });

    $("#modalDetalleMensaje").on('hidden.bs.modal', function (e) {
        cancelarEdicion();
    });

    $("#subirArchivoDet").on('change', function (e) {
        if (this.checked) {
            $("#archivoDet").removeClass('d-none');
        } else {
            $("#archivoDet").addClass('d-none');
            $("#archivoDet").val('')
        }
    });
});

function cargarMensajes(mensajes) {
    let idTabla = 'tabMensajes';
    //$(`#${idTabla} tbody`).empty();
    if ($.fn.DataTable.isDataTable(`#${idTabla}`)) {
        $(`#${idTabla}`).DataTable().clear().destroy();
    }
    let mensajesObj = {};
    for (let men of mensajes) {
        $(`#${idTabla} tbody`).append(`<tr id="mensaje-${men.id}"></tr>`);
        mensajesObj[men.id] = men;
        $(`#mensaje-${men.id}`).append(
            `<td class="texto">${men.codigo}</td>
            <td class="texto">${men.titulo}</td>
            <td class="texto">${men.diagramado == "1" ? 'Sí': 'No'}</td>
            <td class="texto">${men.paginas}</td>
            <td class="texto">${men.tabloides}</td>
            <td class="url">${men.archivo ? '<a href="' + men.archivo + '" target="_blank">Ver archivo</a>': 'No disponible'}</td>
            <td class="acciones">
                <button class="btn btn-primary btn-sm btn-fab btn-icon btn-round"
                    onclick='mostrarDetalleMensaje(this, ${men.id})' data-target="tooltip" data-placement="left"
                    title="Ver detalles">
                    <i class="fa fa-search"></i>
                </button>
            </td>`
        );
    }
    sessionStorage.setItem('mensajes', JSON.stringify(mensajesObj));
    $('[data-target="tooltip"]').tooltip();
    inicializarDataTable(idTabla).columns(0).order('desc').draw();
}

async function getMensajes() {
    try {
        const response = await fetch(HOST_MENSAJE + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getAutores() {
    try {
        const response = await fetch(HOST_AUTOR + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getLugaresPredicaciones() {
    try {
        const response = await fetch(HOST_LUGAR_PREDICACION + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getEdiciones() {
    try {
        const response = await fetch(HOST_EDICION + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getEstatusImpresion() {
    try {
        const response = await fetch(HOST_ESTATUS_IMPRESION + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getImprentas() {
    try {
        const response = await fetch(HOST_IMPRENTA + "?mode=getAll");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

async function getCategorias() {
    try {
        const response = await fetch(HOST_CATEGORIA + "?mode=getAllMensajes");
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        return response.json();
    }
    catch (error) {
        console.log(error);
        return error;
    }
}

function mostrarDetalleMensaje(elem, idMensaje) {
    let mensajes = JSON.parse(sessionStorage.getItem('mensajes'));
    let men = mensajes[idMensaje];
    $("#idDet").val(men.id);
    $("#codigoDet").val(men.codigo);
    $("#tituloDet").val(men.titulo);
    $("#autorDetTxt").val(men.autor.nombre);
    $("#lugarDetTxt").val(men.lugarPredicacion.nombre);
    $("#categoriaDetTxt").val(men.categoria.descripcion);
    $("#fechaDet").val(men.fechaPredicacion);
    $("#edicionDetTxt").val(men.edicion.nombre);
    $("#estatusDetTxt").val(men.estatusImpresion.descripcion);
    $("#diagramadoDetTxt").val(men.diagramado == "1" ? 'Sí': 'No');
    $("#paginasDet").val(men.paginas);
    $("#tabloidesDet").val(men.tabloides);
    $("#ejemplaresDet").val(men.ejemplares);
    $("#imprentaDetTxt").val(men.imprenta.nombre);
    if (men.archivo) {
        let archivo_split = men.archivo.split('/');
        let nombre_archivo = archivo_split[archivo_split.length - 1];
        $("#nombreArchivoDet").html(`<a href="${men.archivo}" target="_blank">${nombre_archivo}</a>`)
    } else {
        $("#nombreArchivoDet").text("No disponible");
    }
    $("#archivoDet").addClass("d-none");

    $("#modalDetalleMensaje").modal('toggle');
}

function registrarMensaje(elem) {
    $("#modalRegistrarMensaje").modal('toggle');
}

function editarMensaje() {
    $("#modalDetalleMensaje input.editable").each(function (index, input) {
        input["data-old"] = input.value;
        $(input).prop("readonly", false);
    });
    $("#modalDetalleMensaje input.selectable").each(function (index, input) {
        let idSelect = input.id.substring(0, input.id.indexOf("Txt"));
        $(`#${idSelect} option`).each(function (index, option) {
            if ($(option).text() === input.value) {
                $(option).prop("selected", true);
                return false;
            }
        });
        $(`#${idSelect}, #${input.id}`).toggleClass("d-none");
    });
    $("#subirNuevoArchivoDiv").removeClass('d-none');
    $("#modalDetalleMensaje .modal-footer button").toggleClass("d-none");
}

function cancelarEdicion() {
    $("#modalDetalleMensaje input.editable").each(function (index, input) {
        input.value = input["data-old"];
        $(input).prop("readonly", true);
    });
    $("#modalDetalleMensaje input.selectable").each(function (index, input) {
        let idSelect = input.id.substring(0, input.id.indexOf("Txt"));
        $(`#${idSelect}`).addClass("d-none");
        $(`#${input.id}`).removeClass("d-none");
    });
    $("#btnEliminarMensaje, #btnEditarMensaje, #nombreArchivoDet").removeClass("d-none");
    $("#btnCancelarEdicion, #btnGuardarCambios, #archivoDet, #subirNuevoArchivoDiv").addClass("d-none");
    $("#subirArchivoDet").prop('checked', false);
    $("#archivoDet").val('');
}

function eliminarMensaje() {
    let accionSiExito = function () {
        getMensajes().then(mensajes => {
            cargarMensajes(mensajes);
            alertarExito('¡Éxito!', 'El mensaje fue eliminado exitosamente');
            $("#modalDetalleMensaje").modal('hide');
        });
    };
    let accionAjax = function () {
        let id = $(`#idDet`).val();
        return fetch(HOST_MENSAJE + `?mode=delete&id=${id}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText)
                }
                return response.json()
            })
            .then(respJson => {
                if (respJson.error) {
                    Swal.showValidationMessage(
                        `Request failed: ${respJson.message}`
                    );
                } else {
                    return respJson;
                }
            })
            .catch(error => {
                Swal.showValidationMessage(
                    `Request failed: ${error}`
                )
            });
    };
    confirmarAlerta(
        '¿Seguro?',
        '¿Seguro de que desea eliminar este mensaje?',
        accionSiExito,
        accionAjax
    );
}
<?php

include_once 'Autor.php';
include_once 'Categoria.php';
include_once 'Edicion.php';
include_once 'EstatusImpresion.php';
include_once 'Imprenta.php';
include_once 'LugarPredicacion.php';

class Mensaje {
    private $id;
    private $codigo;
    private $titulo;
    private $fechaPredicacion;
    private $ejemplares;
    private $diagramado;
    private $paginas;
    private $tabloides;
    private $fechaRegistro;
    private $estatus;
    private $autor;
    private $categoria;
    private $edicion;
    private $estatusImpresion;
    private $imprenta;
    private $lugarPredicacion;
    private $archivo;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function getFechaPredicacion() {
        return $this->fechaPredicacion;
    }

    public function setFechaPredicacion($fechaPredicacion) {
        $this->fechaPredicacion = $fechaPredicacion;
    }

    public function getEjemplares() {
        return $this->ejemplares;
    }

    public function setEjemplares($ejemplares) {
        $this->ejemplares = $ejemplares;
    }

    public function getDiagramado() {
        return $this->diagramado;
    }

    public function setDiagramado($diagramado) {
        $this->diagramado = $diagramado;
    }

    public function getPaginas() {
        return $this->paginas;
    }

    public function setPaginas($paginas) {
        $this->paginas = $paginas;
    }

    public function getTabloides() {
        return $this->tabloides;
    }

    public function setTabloides($tabloides) {
        $this->tabloides = $tabloides;
    }

    public function getFechaRegistro() {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro($fechaRegistro) {
        $this->fechaRegistro = $fechaRegistro;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function getAutor() {
        return $this->autor;
    }

    public function setAutor($autor) {
        $this->autor = $autor;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function getEdicion() {
        return $this->edicion;
    }

    public function setEdicion($edicion) {
        $this->edicion = $edicion;
    }

    public function getEstatusImpresion() {
        return $this->estatusImpresion;
    }

    public function setEstatusImpresion($estatusImpresion) {
        $this->estatusImpresion = $estatusImpresion;
    }

    public function getImprenta() {
        return $this->imprenta;
    }

    public function setImprenta($imprenta) {
        $this->imprenta = $imprenta;
    }

    public function getLugarPredicacion() {
        return $this->lugarPredicacion;
    }

    public function setLugarPredicacion($lugarPredicacion) {
        $this->lugarPredicacion = $lugarPredicacion;
    }

    public function getArchivo() {
        return $this->archivo;
    }

    public function setArchivo($archivo) {
        $this->archivo = $archivo;
    }

    public function expose() {
        return get_object_vars($this);
    }

}

?>
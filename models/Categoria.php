<?php

include_once 'TipoCategoria.php';

class Categoria {
    private $id;
    private $descripcion;
    private $tipoCategoria;
    private $estatus;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function getEstatus() {
        return $this->estatus;
    }

    public function setEstatus($estatus) {
        $this->estatus = $estatus;
    }

    public function getTipoCategoria() {
        return $this->tipoCategoria;
    }

    public function setTipoCategoria($tipoCategoria) {
        $this->tipoCategoria = $tipoCategoria;
    }

    public function expose() {
        return get_object_vars($this);
    }

}

?>
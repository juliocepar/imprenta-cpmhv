<?php

require_once 'Config.php';
require_once '../models/Autor.php';

class AutorDAO {

    private $db;
    private static $tableName = 'autor';

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public function insert(Autor $objeto) {
        $objeto = $objeto->expose();
        $arreglo = [];
        foreach ($objeto as $atr => $valor) {
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->insert(self::$tableName, $arreglo);
    }

    public function getAll() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE estatus = 1 ORDER BY id");
        foreach ($resultSet as $row) {
            $objeto = new Autor();
            $objeto->setId($row->id);
            $objeto->setNombre($row->nombre);
            $objeto->setEstatus($row->estatus);
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getAllSinFiltros() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " ORDER BY id");
        foreach ($resultSet as $row) {
            $objeto = new Autor();
            $objeto->setId($row->id);
            $objeto->setNombre($row->nombre);
            $objeto->setEstatus($row->estatus);
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getById(int $id) {
        $resultSet = $this->db->get_row("SELECT * FROM " . self::$tableName . " WHERE id = $id");
        $objeto = null;
        if (isset($resultSet)) {
            $objeto = new Autor();
            $objeto->setId($resultSet->id);
            $objeto->setNombre($resultSet->nombre);
            $objeto->setEstatus($resultSet->estatus);
        }
        return $objeto ? $objeto->expose() : null;
    }

    public function update(Autor $object) {
        $objeto = $object->expose();
        $arreglo = [];
        foreach ($objeto as $atr => $valor) {
            if ($atr === 'id') {
                continue;
            }
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->update(self::$tableName, $arreglo, ['id' => $object->getId()]);
    }

    public function deleteL(int $id) {
        return $this->db->update(self::$tableName, ['estatus' => 0], ['id' => $id]);
    }

}

?>
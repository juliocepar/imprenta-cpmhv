<?php

require_once 'Config.php';
require_once '../models/TipoCategoria.php';

class TipoCategoriaDAO {

    private $db;
    private static $tableName = 'tipo_categoria';

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public function insert(TipoCategoria $objeto) {
        return $this->db->insert(self::$tableName, $objeto);
    }

    public function getAll() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE estatus = 1 ORDER BY id");
        foreach ($resultSet as $cat) {
            $objeto = new TipoCategoria();
            $objeto->setId($cat->id);
            $objeto->setDescripcion($cat->descripcion);
            $objeto->setEstatus($cat->estatus);
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getAllSinFiltros() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " ORDER BY id");
        foreach ($resultSet as $cat) {
            $objeto = new TipoCategoria();
            $objeto->setId($cat->id);
            $objeto->setDescripcion($cat->descripcion);
            $objeto->setEstatus($cat->estatus);
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }
    
    public function getById(int $id) {
        $resultSet = $this->db->get_row("SELECT * FROM " . self::$tableName . " WHERE id = $id");
        $objeto = null;
        if (isset($resultSet)) {
            $objeto = new TipoCategoria();
            $objeto->setId($resultSet->id);
            $objeto->setDescripcion($resultSet->descripcion);
            $objeto->setEstatus($resultSet->estatus);
        }
        return $objeto->expose();
    }

    public function update($values, $where) {
        return $this->db->update(self::$tableName, $values, $where);
    }

    public function deleteL(int $id) {
        return $this->db->update(self::$tableName, ['estatus' => 0], ['id' => $id]);
    }

}

?>
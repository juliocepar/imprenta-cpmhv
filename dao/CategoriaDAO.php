<?php

require_once 'Config.php';
require_once '../models/Categoria.php';
require_once 'TipoCategoriaDAO.php';

class CategoriaDAO {

    private $db;
    private static $tableName = 'categoria';

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public function insert(Categoria $objeto) {
        $categoria = $objeto->expose();
        $arreglo = [];
        foreach ($categoria as $atr => $valor) {
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->insert(self::$tableName, $arreglo);
    }

    public function getAll($idTipoCategoria) {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE id_tipo_categoria = $idTipoCategoria AND estatus = 1 ORDER BY id");
        $tipoCategoriaDAO = new TipoCategoriaDAO();
        $tiposCategorias = $tipoCategoriaDAO->getAll();
        foreach ($resultSet as $cat) {
            $objeto = new Categoria();
            $objeto->setId($cat->id);
            $objeto->setDescripcion($cat->descripcion);
            $objeto->setEstatus($cat->estatus);
            foreach ($tiposCategorias as $tipoCat) {
                if ($tipoCat['id'] == $cat->id_tipo_categoria) {
                    $objeto->setTipoCategoria($tipoCat);
                    break;
                }
            }
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getAllSinDiscriminarTipo() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE estatus = 1 ORDER BY id");
        $tipoCategoriaDAO = new TipoCategoriaDAO();
        $tiposCategorias = $tipoCategoriaDAO->getAll();
        foreach ($resultSet as $cat) {
            $objeto = new Categoria();
            $objeto->setId($cat->id);
            $objeto->setDescripcion($cat->descripcion);
            $objeto->setEstatus($cat->estatus);
            foreach ($tiposCategorias as $tipoCat) {
                if ($tipoCat['id'] == $cat->id_tipo_categoria) {
                    $objeto->setTipoCategoria($tipoCat);
                    break;
                }
            }
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getAllSinFiltros($idTipoCategoria) {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE id_tipo_categoria = $idTipoCategoria ORDER BY id");
        $tipoCategoriaDAO = new TipoCategoriaDAO();
        $tiposCategorias = $tipoCategoriaDAO->getAllSinFiltros();
        foreach ($resultSet as $cat) {
            $objeto = new Categoria();
            $objeto->setId($cat->id);
            $objeto->setDescripcion($cat->descripcion);
            $objeto->setEstatus($cat->estatus);
            foreach ($tiposCategorias as $tipoCat) {
                if ($tipoCat['id'] == $cat->id_tipo_categoria) {
                    $objeto->setTipoCategoria($tipoCat);
                    break;
                }
            }
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getById(int $id) {
        $resultSet = $this->db->get_row("SELECT * FROM " . self::$tableName . " WHERE id = $id");
        $objeto = null;
        if (isset($resultSet)) {
            $objeto = new Categoria();
            $objeto->setId($resultSet->id);
            $objeto->setDescripcion($resultSet->descripcion);
            $objeto->setEstatus($resultSet->estatus);
            $tipoCategoriaDAO = new TipoCategoriaDAO();
            $tiposCategorias = $tipoCategoriaDAO->getAllSinFiltros();
            foreach ($tiposCategorias as $tipoCat) {
                if ($tipoCat['id'] == $resultSet->id_tipo_categoria) {
                    $objeto->setTipoCategoria($tipoCat);
                    break;
                }
            }
        }
        return $objeto->expose();
    }

    public function update(Categoria $object) {
        $categoria = $object->expose();
        $arreglo = [];
        foreach ($categoria as $atr => $valor) {
            if ($atr === 'id') {
                continue;
            }
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->update(self::$tableName, $arreglo, ['id' => $object->getId()]);
    }

    public function deleteL(int $id) {
        return $this->db->update(self::$tableName, ['estatus' => 0], ['id' => $id]);
    }

}

?>
<?php

require_once 'Config.php';
require_once '../models/Imprenta.php';

class ImprentaDAO {

    private $db;
    private static $tableName = 'imprenta';

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public function insert(Imprenta $objeto) {
        $objeto = $objeto->expose();
        $arreglo = [];
        foreach ($objeto as $atr => $valor) {
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->insert(self::$tableName, $arreglo);
    }

    public function getAll() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE estatus = 1 ORDER BY id");
        foreach ($resultSet as $row) {
            $objeto = new Imprenta();
            $objeto->setId($row->id);
            $objeto->setCodigo($row->codigo);
            $objeto->setNombre($row->nombre);
            $objeto->setEstatus($row->estatus);
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getAllSinFiltros() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " ORDER BY id");
        foreach ($resultSet as $row) {
            $objeto = new Imprenta();
            $objeto->setId($row->id);
            $objeto->setCodigo($row->codigo);
            $objeto->setNombre($row->nombre);
            $objeto->setEstatus($row->estatus);
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getById(int $id) {
        $resultSet = $this->db->get_row("SELECT * FROM " . self::$tableName . " WHERE id = $id");
        $objeto = null;
        if (isset($resultSet)) {
            $objeto = new Imprenta();
            $objeto->setId($resultSet->id);
            $objeto->setCodigo($resultSet->codigo);
            $objeto->setNombre($resultSet->nombre);
            $objeto->setEstatus($resultSet->estatus);
        }
        return $objeto ? $objeto->expose() : null;
    }

    public function update(Imprenta $object) {
        $objeto = $object->expose();
        $arreglo = [];
        foreach ($objeto as $atr => $valor) {
            if ($atr === 'id') {
                continue;
            }
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->update(self::$tableName, $arreglo, ['id' => $object->getId()]);
    }

    public function deleteL(int $id) {
        return $this->db->update(self::$tableName, ['estatus' => 0], ['id' => $id]);
    }

}

?>
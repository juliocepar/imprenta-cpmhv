<?php

require_once 'Config.php';
require_once '../models/Publicacion.php';
require_once 'CategoriaDAO.php';
require_once 'ImprentaDAO.php';
require_once 'EstatusImpresionDAO.php';

class PublicacionDAO {

    private $db;
    private static $tableName = 'publicacion';

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public function insert(Publicacion $objeto) {
        $mensaje = $objeto->expose();
        $arreglo = [];
        foreach ($mensaje as $atr => $valor) {
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->insert(self::$tableName, $arreglo);
    }

    public function getAll() {
        $arreglo = [];
        $resultSet = $this->db->get_results("SELECT * FROM " . self::$tableName . " WHERE estatus = 1 ORDER BY id");
        $categoriaDAO = new CategoriaDAO();
        $categorias = $categoriaDAO->getAllSinFiltros(2);
        $estatusImpresionDAO = new EstatusImpresionDAO();
        $estatusImpresion = $estatusImpresionDAO->getAllSinFiltros();
        $imprentaDAO = new ImprentaDAO();
        $imprentas = $imprentaDAO->getAllSinFiltros();
        foreach ($resultSet as $men) {
            $objeto = new Publicacion();
            $objeto->setId($men->id);
            $objeto->setTitulo($men->titulo);
            $objeto->setEjemplares($men->ejemplares);
            $objeto->setDiagramado($men->diagramado);
            $objeto->setPaginas($men->paginas);
            $objeto->setTabloides($men->tabloides);
            $objeto->setFechaRegistro($men->fecha_registro);
            $objeto->setEstatus($men->estatus);
            $objeto->setArchivo($men->archivo);
            foreach ($categorias as $categoria) {
                if ($categoria['id'] == $men->id_categoria) {
                    $objeto->setCategoria($categoria);
                    break;
                }
            }
            foreach ($imprentas as $imprenta) {
                if ($imprenta['id'] == $men->id_imprenta) {
                    $objeto->setImprenta($imprenta);
                    break;
                }
            }
            foreach ($estatusImpresion as $estImp) {
                if ($estImp['id'] == $men->id_estatus_impresion) {
                    $objeto->setEstatusImpresion($estImp);
                    break;
                }
            }
            array_push($arreglo, $objeto->expose());
        }
        return $arreglo;
    }

    public function getById(int $id) {
        $men = $this->db->get_row("SELECT * FROM " . self::$tableName . " WHERE id = $id");
        $objeto = null;
        if (isset($men)) {
            $categoriaDAO = new CategoriaDAO();
            $categorias = $categoriaDAO->getAllSinFiltros(2);
            $estatusImpresionDAO = new EstatusImpresionDAO();
            $estatusImpresion = $estatusImpresionDAO->getAllSinFiltros();
            $imprentaDAO = new ImprentaDAO();
            $imprentas = $imprentaDAO->getAllSinFiltros();

            $objeto = new Publicacion();
            $objeto->setId($men->id);
            $objeto->setTitulo($men->titulo);
            $objeto->setEjemplares($men->ejemplares);
            $objeto->setDiagramado($men->diagramado);
            $objeto->setPaginas($men->paginas);
            $objeto->setTabloides($men->tabloides);
            $objeto->setFechaRegistro($men->fecha_registro);
            $objeto->setEstatus($men->estatus);
            $objeto->setArchivo($men->archivo);
            foreach ($categorias as $categoria) {
                if ($categoria['id'] == $men->id_categoria) {
                    $objeto->setCategoria($categoria);
                    break;
                }
            }
            foreach ($imprentas as $imprenta) {
                if ($imprenta['id'] == $men->id_imprenta) {
                    $objeto->setImprenta($imprenta);
                    break;
                }
            }
            foreach ($estatusImpresion as $estatusImpresion) {
                if ($estatusImpresion['id'] == $men->id_estatus_impresion) {
                    $objeto->setEstatusImpresion($estatusImpresion);
                    break;
                }
            }
        }
        return $objeto ? $objeto->expose() : null;
    }

    public function update(Publicacion $object) {
        $categoria = $object->expose();
        $arreglo = [];
        foreach ($categoria as $atr => $valor) {
            if ($atr === 'id') {
                continue;
            }
            $column = $this->db->fromCamelCaseToSnakeCase($atr);
            if (gettype($valor) != 'array') {
                $arreglo[$column] = $valor;
            } else {
                $arreglo["id_$column"] = $valor['id'];
            }
        }
        return $this->db->update(self::$tableName, $arreglo, ['id' => $object->getId()]);
    }

    public function deleteL(int $id) {
        return $this->db->update(self::$tableName, ['estatus' => 0], ['id' => $id]);
    }

}

?>
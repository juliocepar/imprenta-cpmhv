<?php

require_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 1));
$dotenv->load();

$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

/* Clase encargada de gestionar las conexiones a la base de datos */
Class Db {

    const _NULL = "NULL";
    const _CURRENT_DATE = "CURRENT_DATE";
    const _CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP";

    private $servidor;
    private $usuario;
    private $password;
    private $base_datos;
    private $puerto;
    private $mysqli;

    private static $_instance;

    /*La función construct es privada para evitar que el objeto pueda ser creado mediante new*/
    private function __construct() {
        $this->connect();
        set_error_handler("self::myFunctionErrorHandler", E_WARNING);
    }

    /*Evitamos el clonaje del objeto. Patrón Singleton*/
    private function __clone() {}

    /*Función encargada de crear, si es necesario, el objeto. Esta es la función que debemos llamar desde fuera de la clase para instanciar el objeto, y así, poder utilizar sus métodos*/
    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /*Realiza la conexión a la base de datos.*/
    private function connect() {
        $this->servidor = getenv('DB_HOST');
        $this->usuario = getenv('DB_USER');
        $this->password = getenv('DB_PASS');
        $this->base_datos = getenv('DB_NAME');
        $this->puerto = getenv('DB_PORT') ?: null;
        $this->mysqli = new mysqli($this->servidor, $this->usuario, $this->password, $this->base_datos, $this->puerto);
        if ($this->mysqli->connect_errno) {
            die("Conexión falló: " . $this->mysqli->connect_error);
        }
        $this->mysqli->set_charset("utf8");
    }

    /*Método para ejecutar una sentencia sql genérica*/
    public function query($sql) {
        return $this->mysqli->query($sql);
    }

    /*Método para ejecutar un sentencia sql de tipo SELECT y retornar sus filas como un arreglo */
    public function get_results($sql) {
        $arrayObject = [];
        $result = $this->query($sql);
        while ($obj = $result->fetch_object()) {
            array_push($arrayObject, $obj);
        }
        return $arrayObject;
    }

    /*Método para ejecutar un sentencia sql de tipo SELECT y retornar solo una fila */
    public function get_row($sql) {
        return $this->query($sql)->fetch_object();
    }

    /*Método para construir una sentencia INSERT sql*/
    public function insert($tabla, $arrayValores) {
        $sql = "INSERT INTO $tabla " . $this->buildInsertColumnsValues($arrayValores);
        if ($this->query($sql)) {
            return $this->lastID();
        }
        return false;
    }

    /*Devuelve el último id autoincrementado registrado por un INSERT*/
    private function lastID() {
        return $this->mysqli->insert_id;
    }

    /*Función auxiliar para generar los valores por insertar para ser concatenados*/
    private function buildInsertColumnsValues($assocArray) {
        if (!isset($assocArray) || count($assocArray) == 0) {
            throw new Exception("Arreglo de valores a insertar no está inicializado o está vacío");
        }
        $delimiter = "";
        $fields = ""; // Columnas
        $values = ""; // Valores de cada columna
        foreach ($assocArray as $key => $value) {
            $fields .= $delimiter . $key;
            if (!isset($value)) {
                $values .= $delimiter . self::_NULL;
            } elseif (is_string($value) && is_null(constant("self::_$value"))) {
                $values .= $delimiter . "'$value'";
            } else {
                $values .= $delimiter . $value;
            }
            $delimiter = ", ";
        }
        return "($fields) VALUES ($values)";
    }

    /*Método para construir una sentencia UPDATE sql*/
    public function update(String $tabla, Array $arrayValores, Array $where) {
        $sql = "UPDATE $tabla SET " . $this->buildUpdateFields($arrayValores) . $this->buildWhere($where);
        return $this->query($sql); // Retorna TRUE si es exitoso, FALSE en caso contrario
    }

    /*Función auxiliar para generar los valores por actualizar para ser concatenados*/
    private function buildUpdateFields($assocArray) {
        if (!isset($assocArray) || count($assocArray) == 0) {
            throw new Exception("Arreglo de valores a actualizar no está inicializado o está vacío");
        }
        $str = "";
        $delimiter = "";
        foreach ($assocArray as $key => $value) {
            if (!isset($value)) {
                $str .= $delimiter . "$key = " . self::_NULL;
            } elseif (is_string($value) && is_null(constant("self::_$value"))) {
                $str .= $delimiter . "$key = '$value'";
            } else {
                $str .= $delimiter . "$key = $value";
            }
            $delimiter = ", ";
        }
        return $str;
    }

    /*Función auxiliar para generar las condiciones de la consulta (WHERE) para ser concatenados*/
    // Acepta un arreglo vacío o null, en cuyo caso retorna el string ""
    private function buildWhere($assocArray) {
        if (!isset($assocArray) || count($assocArray) == 0) {
            return "";
        }
        $str = "";
        $delimiter = " WHERE ";
        foreach ($assocArray as $key => $value) {
            if (!isset($value)) {
                $str .= $delimiter . "$key IS " . self::_NULL;
            } elseif (is_string($value) && is_null(constant("self::_$value"))) {
                $str .= $delimiter . "$key = '$value'";
            } else {
                $str .= $delimiter . "$key = $value";
            }
            $delimiter = " AND ";
        }
        return $str;
    }

    /*Método para construir una sentencia DELETE sql*/
    public function delete($tabla, $where) {
        $sql = "DELETE FROM $tabla" . $this->buildWhere($where);
        return $this->query($sql); // Retorna TRUE si es exitoso, FALSE en caso contrario
    }

    // Con esta función impedimos que se dispare un E_WARNING al evaluar un constant que no existe
    private static function myFunctionErrorHandler($errno, $errstr, $errfile, $errline) {
        /* Según el típo de error, lo procesamos */
        switch ($errno) {
        case E_WARNING:
            /* No ejecutar el gestor de errores interno de PHP, hacemos que lo pueda procesar un try catch */
            return true;
            break;

        default:
            /* Ejecuta el gestor de errores interno de PHP */
            return false;
            break;
        }
    }

    public function fromCamelCaseToSnakeCase($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

}

?>

-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-12-2019 a las 23:45:55
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `imprenta_cpmhv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autor`
--

CREATE TABLE `autor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `autor`
--

INSERT INTO `autor` (`id`, `nombre`, `estatus`) VALUES
(1, 'William Marrion Branham', 1),
(2, 'Julius Stadsklev', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `id_tipo_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `descripcion`, `estatus`, `id_tipo_categoria`) VALUES
(1, 'Predicaciones', 1, 1),
(2, 'Estudios', 1, 1),
(3, 'Compendios', 1, 2),
(4, 'Tratados', 1, 2),
(5, 'Himnarios', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edicion`
--

CREATE TABLE `edicion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `edicion`
--

INSERT INTO `edicion` (`id`, `nombre`, `estatus`) VALUES
(1, 'La Palabra Hablada', 1),
(2, 'Quitando el Velo', 1),
(3, 'El Compendio', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus_impresion`
--

CREATE TABLE `estatus_impresion` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estatus_impresion`
--

INSERT INTO `estatus_impresion` (`id`, `descripcion`, `estatus`) VALUES
(1, 'Impreso', 1),
(2, 'En proceso', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imprenta`
--

CREATE TABLE `imprenta` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imprenta`
--

INSERT INTO `imprenta` (`id`, `codigo`, `nombre`, `estatus`) VALUES
(1, 'ICPMH/V', 'Imprenta del Compañerismo de Pastores del Mensaje de la Hora - Venezuela', 1),
(2, 'VGR', 'Grabaciones La Voz de Dios', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar_predicacion`
--

CREATE TABLE `lugar_predicacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lugar_predicacion`
--

INSERT INTO `lugar_predicacion` (`id`, `nombre`, `estatus`) VALUES
(1, 'Jeffersonville, Indiana, EE. UU.', 1),
(2, 'Barquisimeto, Venezuela', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE `mensaje` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `fecha_predicacion` date NOT NULL,
  `ejemplares` int(11) NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `id_estatus_impresion` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_edicion` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `id_lugar_predicacion` int(11) NOT NULL,
  `id_imprenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE `publicacion` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `ejemplares` int(11) NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `id_categoria` int(11) NOT NULL,
  `id_imprenta` int(11) NOT NULL,
  `id_estatus_impresion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_categoria`
--

CREATE TABLE `tipo_categoria` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_categoria`
--

INSERT INTO `tipo_categoria` (`id`, `descripcion`, `estatus`) VALUES
(1, 'Mensajes', 1),
(2, 'Otras publicaciones', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_categoria_tipo_categoria` (`id_tipo_categoria`);

--
-- Indices de la tabla `edicion`
--
ALTER TABLE `edicion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estatus_impresion`
--
ALTER TABLE `estatus_impresion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imprenta`
--
ALTER TABLE `imprenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`codigo`);

--
-- Indices de la tabla `lugar_predicacion`
--
ALTER TABLE `lugar_predicacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`codigo`),
  ADD KEY `fk_autor` (`id_autor`),
  ADD KEY `fk_categoria` (`id_categoria`),
  ADD KEY `fk_edicion` (`id_edicion`),
  ADD KEY `fk_estatus_impresion` (`id_estatus_impresion`),
  ADD KEY `fk_imprenta` (`id_imprenta`),
  ADD KEY `fk_lugar_predicacion` (`id_lugar_predicacion`);

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_publicacion_categoria` (`id_categoria`),
  ADD KEY `fk_publicacion_estatus_impresion` (`id_estatus_impresion`),
  ADD KEY `fk_publicacion_imprenta` (`id_imprenta`);

--
-- Indices de la tabla `tipo_categoria`
--
ALTER TABLE `tipo_categoria`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autor`
--
ALTER TABLE `autor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `edicion`
--
ALTER TABLE `edicion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estatus_impresion`
--
ALTER TABLE `estatus_impresion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `imprenta`
--
ALTER TABLE `imprenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `lugar_predicacion`
--
ALTER TABLE `lugar_predicacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_categoria`
--
ALTER TABLE `tipo_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `FK_categoria_tipo_categoria` FOREIGN KEY (`id_tipo_categoria`) REFERENCES `tipo_categoria` (`id`);

--
-- Filtros para la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD CONSTRAINT `fk_autor` FOREIGN KEY (`id_autor`) REFERENCES `autor` (`id`),
  ADD CONSTRAINT `fk_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `fk_edicion` FOREIGN KEY (`id_edicion`) REFERENCES `edicion` (`id`),
  ADD CONSTRAINT `fk_estatus_impresion` FOREIGN KEY (`id_estatus_impresion`) REFERENCES `estatus_impresion` (`id`),
  ADD CONSTRAINT `fk_imprenta` FOREIGN KEY (`id_imprenta`) REFERENCES `imprenta` (`id`),
  ADD CONSTRAINT `fk_lugar_predicacion` FOREIGN KEY (`id_lugar_predicacion`) REFERENCES `lugar_predicacion` (`id`);

--
-- Filtros para la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD CONSTRAINT `fk_publicacion_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `fk_publicacion_estatus_impresion` FOREIGN KEY (`id_estatus_impresion`) REFERENCES `estatus_impresion` (`id`),
  ADD CONSTRAINT `fk_publicacion_imprenta` FOREIGN KEY (`id_imprenta`) REFERENCES `imprenta` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
